[//]: # (Topic: Matter Process Lab Github Research)
[//]: # (Author: Tyrone Marshall)
[//]: # (Date: 2016.04.04)
[//]: # (Format: markdown)
[//]: # (Version 2016)

![logo](https://bitbucket.org/processlabdev/daenvw/raw/master/images/daylight.png)
#DaEnVw
A Process Lab Research Private Github Work
***
Process Lab proposes a repository for improving the design process for the analysis of light quality, predicted energy use, and access for views and daylight.